<a name="init-0.0.1"></a>
## 0.0.1 (2021-03-20)


### Creation

* **Setup CI/CD:** Added pipeline to deploy on Gitlab pages ([#8248faea
](https://gitlab.com/psilore/daedalus/-/issues/12)) ([8248faea](https://gitlab.com/psilore/daedalus/-/commit/51642a471c04b9779a97da41759f211791fbfba5))
* **Made project available publicly:** Automatic deploy to Gitlab pages ([#51642a47
](https://gitlab.com/psilore/daedalus/-/issues/11)) ([51642a47](https://gitlab.com/psilore/daedalus/-/tree/8248faeaa1601ae2c3292214eb19ce3ad81c9092))
