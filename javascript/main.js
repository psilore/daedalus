const API_KEY = "api-key 7:2021-03-18:lucas.stenberg@tretton37.com 4d6e422c1a1f7542724c9eacefaba90ea80dbf0164e91703cf52060e8df6b482";
const employees = "https://api.1337co.de/v3/employees";
const assassins = document.getElementById('assassins');
const listBtn = document.getElementById('listBtn');
listBtn.addEventListener("click", setClassList );

const gridBtn = document.getElementById('gridBtn');
gridBtn.addEventListener("click", setClassGrid );
gridBtn.focus();

const authenticatedHeaders = new Headers();
authenticatedHeaders.append('Content-Type', 'application/json');
authenticatedHeaders.append('Authorization', API_KEY);

const requestEmployees = new Request(employees, {
  method: 'GET',
  headers: authenticatedHeaders,
  mode: 'cors',
  cache: 'default',
});


async function fetchAssassins(request) {
    await fetch(request)
    .then(function (data) {

        // Return the response as JSON
        
        return data.json();

    }).then(function (data) {

        data.forEach(function(user){
            let card = '<li class="card" tabindex="0" aria-label="Employee">' +
                '<div class="portrait">' +
                    '<img src="' + user.imagePortraitUrl + '" />' +
                '</div>' +
                '<div class="meta">' +
                    '<div class="data">' + 
                        '<h4 class="name">' + user.name + '</h4>' +
                        '<div class="office">' + user.office + '</div>' +
                    '</div>' +
                    '<div class="social">' +
                        `${user.stackOverflow ? '<span class="icon stack-overflow margin-right-8"></span>' : ''}` +
                        `${user.gitHub ? '<span class="icon github margin-right-8"></span>' : ''}` + 
                        `${user.twitter ? '<span class="icon twitter margin-right-8"></span>' : ''}` +
                        `${user.linkedIn ? '<span class="icon linkedin"></span>' : ''}` +
                    '</div>' +
                '</div>' +
            '</li>';
            if(user.published != false && user.imagePortraitUrl != null) {
                assassins.insertAdjacentHTML('beforeend', card);
            } else {
            // ??
            // console.log('Not a published user or null values in object', user);
            }
        });

    }).catch(function (err) {

        // Log any errors
        console.log('something went wrong', err);

    });
}

fetchAssassins(requestEmployees);

function setClassList() {
  assassins.classList.remove('grid')
  assassins.classList.add('list')
}
function setClassGrid() {
  assassins.classList.remove('list')
  assassins.classList.add('grid')
}