<p align="center">
  <a href="https://psilore.gitlab.io/daedalus/">
    <img src="images/daedalus-solitaire.png" alt="Daedalus solitaire stone" width="96" height="96">
  </a>

  <h3 align="center">Daedalus</h3>

  <p align="center">
    Simpler approach to assignment projects, with jekyll, gitlab
    <br />
    <a href="https://psilore.gitlab.io/daedalus/">View Demo</a>
    ·
    <a href="https://gitlab.com/psilore">Author: Erik Emmerfors</a>
  </p>
</p>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about">About</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>

## About
[![Daedalus screenshot][screenshot]](https://psilore.gitlab.io/daedalus)

Daedalus is an assignment project to show different skill levels, "design/accessibility", "functionality" & "testing"
### Built With

Out of simplicity and maintain speed, simpler tools where chosen. CI/CD is used within Gitlab.
* [Gitlab](https://gitlab.com)
* [Jekyll](https://jekyllrb.com)
* [Sketch](https://www.sketch.com)
* [Icon Slate](https://www.kodlian.com/apps/icon-slate)
* [Visual studio IDE](https://visualstudio.microsoft.com)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
## Getting Started
First step is to install Jekyll then download or clone the repository so you can work on it locally on your client.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

1. Install jekyll

  ```sh
  gem install bundler jekyll
  ```

### Installation

1. Clone the daedalus repository `git@gitlab.com:psilore/daedalus.git`

  ```sh
  git clone git@gitlab.com:psilore/daedalus.git
  ```

2. Change to the root directory of the daedalus project

  ```sh
  $ cd daedalus/
  ➜  daedalus git:(master)
  ```

3. Start local development

  ```sh
  gem install bundler
  bundle install
  bundle exec jekyll serve
  ```

**Example of output**
[![Dev server example output][screenshot-dev-server]](#installation)

[screenshot-dev-server]: images/example-output-local-dev-server.png
[screenshot]: images/screenshot.png